<?php

namespace App\Http\Controllers\Users;

use App\Model\User\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Session;
use Hash;
use Yajra\DataTables\DataTables;

class UserController extends Controller
{
    /* This method use for login view */

    public function index()
    {
        return view('users.login');
    }  
    
    /* This method use for user registration view */

    public function registration()
    {
        return view('users.registration');
    }
      
    /* This method use for login for admin and user */

    public function postLogin(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);
   
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            $is_admin = Auth::user()->is_admin;
            if($is_admin == '1'){
                return redirect()->intended('admin')
                ->withSuccess('You have Successfully loggedin');
            }else{
                return redirect()->intended('dashboard')
                ->withSuccess('You have Successfully loggedin');
            }
        }
  
        return redirect("login")->withSuccess('Oppes! You have entered invalid credentials');
    }
      
    /* This method use for user registration */

    public function postRegistration(Request $request)
    {  
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
            'dob' => 'required',
            'gender' => 'required',
            'occupation' => 'required',
            'family_type' => 'required',
            'annual_income' => 'required',
            'last_name' => 'required',
        ]);
           
        $data = $request->all();
        $check = $this->create($data);

        Auth::loginUsingId($check->id); 
         
        return redirect("dashboard")->withSuccess('You have Successfully Registerd');
    }
    
    /* This method use for user dashboard */ 
   
    public function dashboard()
    {
        if(Auth::check()){
            if(Auth::user()->is_admin == 1){
                return redirect('/admin');
            }
            $user_list = User::where('is_admin',0)->where('gender','!=',Auth::user()->gender)->get();
            $match = 0;
            $user_details = User::where('id',Auth::user()->id)->first();
            foreach ($user_list as $key => $value) {
                if($value->manglik == Auth::user()->manglik){
                    $match = $match+1;
                }
                
                $user_family_type  = explode(',', Auth::user()->family_type);
                $match_family_type = explode(',', $value->family_type);
                $user_occupation   = explode(',', Auth::user()->occupation);
                $match_occupation  = explode(',', $value->occupation);

                for ($i=0; $i < count($user_family_type); $i++) { 
                    if(in_array($user_family_type[$i],$match_family_type)){
                          if($i == 0){
                            $match = $match+1;
                          }  
                    }
                }

                for ($i=0; $i < count($user_occupation); $i++) { 
                    if(in_array($user_occupation[$i],$match_occupation)){
                        if($i == 0){
                            $match = $match+1;
                        }
                    }
                }

                $user_list[$key]->match_percentage =  round(($match*100)/3);
                $match = 0;
            }

            $user_list_array = $user_list->toArray();
            usort($user_list_array, function ($item1, $item2) {
                return $item2['match_percentage'] <=> $item1['match_percentage'];
            });
            return view('users.dashboard',compact('user_list_array','user_details'));
        }
        return redirect("login")->withSuccess('Opps! You do not have access');
    }


    /* This method use for admin dashboard view */

    public function adminDashboard()
    {
        if(Auth::check()){
            if(Auth::user()->is_admin == 0){
                return redirect('/dashboard');
            }
            $user_list = User::where('is_admin',0)->orderBy('created_at','DESC')->get();
            return view('users.admin_dashboard',compact('user_list'));
        }
        return redirect("login")->withSuccess('Opps! You do not have access');
    }

    /* This method use for admin user json data and all filter for admin */

    public function usersJsonData(Request $request){
        $data = $request->all();
        $user_list = User::where('is_admin',0)->orderBy('created_at','DESC');
        if(isset($data['gender']) && !empty($data['gender'])){
                $user_list->where('gender',$data['gender']);
        }
        if(isset($data['family_type']) && !empty($data['family_type'])){
                $user_list->whereRaw('FIND_IN_SET("'.$data['family_type'].'",family_type)');
        }
        if(isset($data['manglik']) && !empty($data['manglik'])){
                $user_list->where('manglik',$data['manglik']);
        }
        if(isset($data['dob']) && !empty($data['dob'])){
                $user_list->where('dob',$data['dob']);
        }
        if(isset($data['reset_filter']) && !empty($data['reset_filter'])){
                
        }
        
        $users =  $user_list;
        return Datatables::of($users)->addIndexColumn()->make(true);
    }

    
   /* This method use for create new user*/
    
    public function create(array $data)
    {
      return User::create([
        'first_name'    => isset($data['first_name']) ? $data['first_name'] : '',
        'last_name'     => isset($data['last_name']) ? $data['last_name'] : '' ,
        'email'         => isset($data['email']) ? $data['email'] : '',
        'password'      => Hash::make($data['password']),
        'dob'           => date("Y-m-d", strtotime($data['dob'])),
        'gender'        => isset($data['gender']) ? $data['gender'] : '',
        'occupation'    => implode(',', $data['occupation']),
        'family_type'   => implode(',', $data['family_type']),
        'manglik'       => $data['manglik'],
        'annual_income' => (string)$data['annual_income'],
        'is_admin'      => '0',
      ]);
    }
    
    
    /* This method use for user logout */

    public function logout() {
        Session::flush();
        Auth::logout();
  
        return Redirect('login');
    }
}
