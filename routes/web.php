<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Users\UserController;


Route::get('/login', function () {
	return redirect('/');
});

//================ User Route Start Here ==========================//

Route::get('/', 'Users\UserController@index')->name('login');
Route::post('post-login', 'Users\UserController@postLogin')->name('login.post'); 
Route::get('registration', 'Users\UserController@registration')->name('register');
Route::post('post-registration', 'Users\UserController@postRegistration')->name('register.post'); 
Route::get('dashboard', 'Users\UserController@dashboard'); 
Route::get('logout', 'Users\UserController@logout')->name('logout');

//====================== User Route End Here ======================//


//========================= Admin Route Start Here ===============//

Route::get('admin', 'Users\UserController@adminDashboard');
Route::any('admin/dataTablelists','Users\UserController@usersJsonData' );

//========================= Admin Route End Here ================//