@extends('layout')
  
@section('content')
<style type="text/css">
    
</style>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/3.3.0/select2.css" rel="stylesheet" />
<link href='https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/ui-lightness/jquery-ui.css' rel='stylesheet'>
<main class="login-form">
  <div class="cotainer">
      <div class="row justify-content-center">
          <div class="col-md-8">
              <div class="card">
                  <div class="card-header">Register</div>
                  <div class="card-body">
  
                      <form action="{{ route('register.post') }}" method="POST">
                          {{ csrf_field() }}
                          <div class="form-group row">
                              <label for="name" class="col-md-4 col-form-label text-md-right">First Name</label>
                              <div class="col-md-6">
                                  <input type="text" id="first_name" class="form-control" name="first_name" required autofocus value="{{ old('first_name') }}">
                                  @if ($errors->has('first_name'))
                                      <span class="text-danger">{{ $errors->first('first_name') }}</span>
                                  @endif
                              </div>
                          </div>

                             <div class="form-group row">
                              <label for="name" class="col-md-4 col-form-label text-md-right">Last Name</label>
                              <div class="col-md-6">
                                  <input type="text" id="last_name" class="form-control" name="last_name" required autofocus value="{{ old('last_name') }}">
                                  @if ($errors->has('last_name'))
                                      <span class="text-danger">{{ $errors->first('last_name') }}</span>
                                  @endif
                              </div>
                          </div>
  
                          <div class="form-group row">
                              <label for="email_address" class="col-md-4 col-form-label text-md-right">E-Mail Address</label>
                              <div class="col-md-6">
                                  <input type="text" id="email_address" class="form-control" name="email" required autofocus value="{{ old('email') }}">
                                  @if ($errors->has('email'))
                                      <span class="text-danger">{{ $errors->first('email') }}</span>
                                  @endif
                              </div>
                          </div>
  
                          <div class="form-group row">
                              <label for="password" class="col-md-4 col-form-label text-md-right">Password</label>
                              <div class="col-md-6">
                                  <input type="password" id="password" class="form-control" name="password" required>
                                  @if ($errors->has('password'))
                                      <span class="text-danger">{{ $errors->first('password') }}</span>
                                  @endif
                              </div>
                          </div>

                          <div class="form-group row">
                              <label for="date of birth" class="col-md-4 col-form-label text-md-right">Date Of Birth</label>
                              <div class="col-md-6">
                                  <input type="text" id="my_date_picker" name="dob" class="form-control" required value="{{ old('dob') }}">
                                  @if ($errors->has('dob'))
                                      <span class="text-danger">{{ $errors->first('dob') }}</span>
                                  @endif
                              </div>
                          </div>

                          

                           <div class="form-group row">
                              <label for="gender" class="col-md-4 col-form-label text-md-right">Gender</label>
                              <div class="col-md-6">
                                  <input type="checkbox" name="gender" value="Male" @if(old('gender') == 'Male') checked @endif><span style="margin-left: 4px">Male</span>
                                  <input type="checkbox" name="gender" value="Female" @if(old('gender') == 'Female') checked @endif><span style="margin-left: 4px">Female</span>
                                  @if ($errors->has('gender'))
                                      <br><span class="text-danger">{{ $errors->first('gender') }}</span>
                                  @endif
                              </div>
                          </div>


                          <div class="form-group row">
                              <label for="occupation" class="col-md-4 col-form-label text-md-right">Select Occupation</label>
                              <div class="col-md-6">
                                  <select class="occupation_select" name="occupation[]" multiple="multiple" style="width:300px;display: none;">
                                      <option value="Private job">Private job</option>
                                      <option value="Government Job">Government Job</option>
                                      <option value="Business">Business</option>
                                  </select> 
                                   @if ($errors->has('occupation'))
                                      <br><span class="text-danger">{{ $errors->first('occupation') }}</span>
                                   @endif
                              </div>
                          </div>

                          <div class="form-group row">
                              <label for="family type" class="col-md-4 col-form-label text-md-right">Select Family Type</label>
                              <div class="col-md-6">
                                  <select class="family_select" name="family_type[]" multiple="multiple" style="width:300px;display: none;">
                                      <option value="Joint family">Joint family</option>
                                      <option value="Nuclear family">Nuclear family</option>
                                  </select> 
                                   @if ($errors->has('family_type'))
                                      <br><span class="text-danger">{{ $errors->first('family_type') }}</span>
                                   @endif
                              </div>
                          </div>

                          <div class="form-group row">
                              <label for="manglik" class="col-md-4 col-form-label text-md-right">Select Manglik</label>
                              <div class="col-md-6">
                                  <select class="form-control" name="manglik">
                                      <option value="">Select Manglik</option>
                                      <option value="Yes" @if(old('manglik') == 'Yes') selected @endif>Yes</option>
                                      <option value="No" @if(old('manglik') == 'No') selected @endif>No</option>
                                      <option value="Both" @if(old('manglik') == 'Both') selected @endif>Both</option>
                                  </select> 
                                   @if ($errors->has('manglik'))
                                      <span class="text-danger">{{ $errors->first('manglik') }}</span>
                                   @endif
                              </div>
                          </div>
                          
                          <div class="form-group row">
                            <label for="annul income" class="col-md-4 col-form-label text-md-right">Annual Income(₹)</label>
                              <div class="col-md-6 offset-md-4">
                                  <input class="input-price" type="text" name="from" id="from" value="{{ old('from') }}" placeholder="From" disabled="disabled"/>
                                  <input class="input-price" type="text" name="to" id="to" value="{{ old('to') }}" placeholder="To" disabled="disabled"/>
                                  <input type="hidden" id="annul_income" name="annual_income" vlaue="{{ old('annual_income') }}">
                              <div id="slider"></div>
                               @if ($errors->has('annual_income'))
                                  <span class="text-danger">{{ $errors->first('annual_income') }}</span>
                               @endif
                            </div>
                          </div>
  
                          <div class="col-md-6 offset-md-4">
                              <button type="submit" class="btn btn-primary">
                                  Register
                              </button>
                          </div>
                      </form>
                        
                  </div>
              </div>
          </div>
      </div>
  </div>
</main>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/3.3.0/select2.js"></script>
<script>
   var myData = [0,100000,200000,300000,400000,500000,600000];
   var slider_config = {
      range: true,
      min: 0,
      max: myData.length - 1,
      step: 1,
      slide: function( event, ui ) {
                    $('#from').val( myData[ ui.values[0] ] );
                    $('#to').val( myData[ ui.values[1] ] );
                    var annul_income = myData[ ui.values[0] ]+"-"+myData[ ui.values[1] ];
                    $('#annul_income').val(annul_income);
      },
      create: function() {
              $(this).slider('values',0,0);
              $(this).slider('values',1,myData.length - 1);
      }
    };

    $("#slider").slider(slider_config);
    
    $('.occupation_select').select2();
    $('.family_select').select2();

    $(document).ready(function() {
      $(function() {
          $( "#my_date_picker" ).datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: "c-100:c+100"
          });
      });
    })

</script>


@endsection