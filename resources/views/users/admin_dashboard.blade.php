@extends('layout')
<style type="text/css">
    .dataTables_filter{
        display: none;
    }
    .input-price{
      margin-bottom: 10px;
      padding:4px;
    }
    .range-filter{
       margin-top: 22px;
       margin-left: 4px;
    }
</style>
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">{{ __('Admin Dashboard') }}</div>
                <div class="card-body">
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                </div>
                <div style="padding:10px">
                    <div class="row">
                           <div class="col-sm-3"> 
                                <label>Family Type: </label>
                                <select class="form-control" id='searchByFamilyType'>
                                  <option value="">All</option>
                                  <option value="Joint family">Joint family</option>
                                  <option value="Nuclear family">Nuclear family</option>
                                </select>
                           </div>
                           <div class="col-sm-3"> 
                                <label>Gender: </label>
                                <select class="form-control" id='searchByGender'>
                                  <option value="">All</option>
                                  <option value="Male">Male</option>
                                  <option value="Female">Female</option>
                                </select>
                           </div>
                           <div class="col-sm-3"> 
                                <label>Manglik: </label>
                                <select class="form-control" id='searchByManglik'>
                                  <option value="">All</option>
                                  <option value="Yes">Yes</option>
                                  <option value="No">No</option>
                                  <option value="Both">Both</option>
                                </select>
                           </div>
                           <div class="col-sm-3"> 
                                <label>Age: </label>
                                <input type="text" class="form-control" id="searchByAge" value="">
                           </div> 
                    </div>
                    <div class="range-filter">
                        <div class="row">
                            <div class="col-sm-6">
                                <label>Annual Income: </label>
                                <input class="input-price" type="text" name="from" id="from" value="{{ old('from') }}" placeholder="From" disabled="disabled"/>
                                <input class="input-price" type="text" name="to" id="to" value="{{ old('to') }}" placeholder="To" disabled="disabled"/>
                                <input type="hidden" id="annul_income" name="annual_income" vlaue="{{ old('annual_income') }}">
                                <div id="slider"></div>
                            </div>
                            <div class="col-sm-3">
                                <input type="button" id="reset_filter" value="Reset Filter">
                            </div>    
                        </div>
                    </div>    
                </div>
                <table id="table_id" class="display" style="width:100%">
                    <thead>  
                        <tr>
                            <th>S. No.</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            <th>Date Of Birth</th>
                            <th>Gender</th>
                            <th>Annual Income(₹)</th>
                            <th>Occupation</th>
                            <th>Family Type</th>
                            <th>Manglik</th>
                        </tr>
                    </thead>
                       
                  </table>

            </div>

        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        var DataTable ;
        var v_token = "{{csrf_token()}}";
        DataTable = $('#table_id').DataTable({
        "processing": true,
        "serverSide": true,
        "targets": 'no-sort',
        "scrollX": true,
        "ajax": {
                'type':"Post",
                'headers': {'X-CSRF-Token': v_token},
                'url':'{{ url('/admin/dataTablelists/') }}',
                'data' : function(data){
                    
                    /*Filter By Gender*/
                    var gender = $('#searchByGender').val();
                    data.gender = gender;
                    
                    /*Filter By Fimaly Type*/
                    var family_type = $('#searchByFamilyType').val();
                    data.family_type = family_type;

                    /*Filter By Manglik*/
                    var manglik = $('#searchByManglik').val();
                    data.manglik = manglik;

                    /*Filter By Age*/
                    var dob = $('#searchByAge').val();
                    data.dob = dob;

                    /*Filter By Income*/
                    var annual_income = $('#annul_income').val();
                    data.annual_income = annual_income;

                    var reset_filter = $('#reset_filter').val();
                    data.reset_filter = reset_filter;
                }
            },
            "columns": [
                {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
                {data: 'first_name', name: 'first_name'},
                {data: 'last_name', name: 'last_name'},
                {data: 'email', name: 'email'},
                {data: 'dob', name: 'dob'},
                {data: 'gender', name: 'gender'},
                {data: 'annual_income', name: 'annual_income'},
                {data: 'occupation', name: 'occupation'},
                {data: 'family_type', name: 'family_type'},
                {data: 'manglik', name: 'manglik'},
            ]
        });


        $('#searchByFamilyType').change(function(){
            DataTable.columns(8).search( this.value ).draw();
        });

        $('#searchByGender').change(function(){
             DataTable.columns(5).search( this.value ).draw();
        });
        $('#searchByManglik').change(function(){
             DataTable.columns(9).search( this.value ).draw();
        });

        $('#reset_filter').click(function(){
            $('#searchByGender').val('');
            $('#annul_income').val('');
            $('#searchByAge').val('');
            $('#searchByManglik').val('');
            $('#searchByFamilyType').val('');
            $('#to').val('');
            $('#from').val('');
            DataTable.search('').columns().search('').draw();
        });

        $( "#searchByAge" ).datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'yy-mm-dd',
            yearRange: "c-100:c+100",
            onSelect: function () { DataTable.draw(); }
        });


       var myData = [0,100000,200000,300000,400000,500000,600000];
       var slider_config = {
          range: true,
          min: 0,
          max: myData.length - 1,
          step: 1,
          slide: function( event, ui ) {
                        $('#from').val( myData[ ui.values[0] ] );
                        $('#to').val( myData[ ui.values[1] ] );
                        var annul_income = myData[ ui.values[0] ]+"-"+myData[ ui.values[1] ];
                        DataTable.search(annul_income).draw();
                        $('#annul_income').val(annul_income);
          },
          create: function() {
                  $(this).slider('values',0,0);
                  $(this).slider('values',1,myData.length - 1);
          }
        };

        $("#slider").slider(slider_config);    
    }); 
</script>

@endsection