<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'first_name' => 'Admin',
            'last_name' => 'Admin',
            'email' => 'admin@admin.com',
            'dob' => '1997-02-01',
            'gender' => 'Male',
            'annual_income' => '',
            'occupation' => '',
            'family_type' => '',
            'manglik' => '',
            'is_admin' => '1',
            'password' => bcrypt('123456'),
        ]);
        DB::table('users')->insert([
            'first_name' => 'User1',
            'last_name' => 'User1',
            'email' => 'user1@email.com',
            'dob' => '1997-02-01',
            'gender' => 'Male',
            'annual_income' => '100000-200000',
            'occupation' => 'Private job,Business',
            'family_type' => 'Nuclear family,Joint family',
            'manglik' => 'Yes',
            'is_admin' => '0',
            'password' => bcrypt('123456'),
        ]);
        DB::table('users')->insert([
            'first_name' => 'User2',
            'last_name' => 'User2',
            'email' => 'user2@email.com',
            'dob' => '1993-02-01',
            'gender' => 'Male',
            'annual_income' => '200000-300000',
            'occupation' => 'Private job,Business',
            'family_type' => 'Nuclear family',
            'manglik' => 'Yes',
            'is_admin' => '0',
            'password' => bcrypt('123456'),
        ]);

        DB::table('users')->insert([
            'first_name' => 'User3',
            'last_name' => 'User3',
            'email' => 'user3@email.com',
            'dob' => '1996-03-02',
            'gender' => 'Male',
            'annual_income' => '300000-400000',
            'occupation' => 'Private job,Government Job,Business',
            'family_type' => 'Nuclear family',
            'manglik' => 'Yes',
            'is_admin' => '0',
            'password' => bcrypt('123456'),
        ]);
        DB::table('users')->insert([
            'first_name' => 'User4',
            'last_name' => 'User4',
            'email' => 'user4@email.com',
            'dob' => '1994-02-04',
            'gender' => 'Male',
            'annual_income' => '400000-500000',
            'occupation' => 'Private job',
            'family_type' => 'Nuclear family,Joint family',
            'manglik' => 'No',
            'is_admin' => '0',
            'password' => bcrypt('123456'),
        ]);
        DB::table('users')->insert([
            'first_name' => 'User5',
            'last_name' => 'User5',
            'email' => 'user5@email.com',
            'dob' => '1995-06-02',
            'gender' => 'Male',
            'annual_income' => '500000-600000',
            'occupation' => 'Private job',
            'family_type' => 'Nuclear family',
            'manglik' => 'Yes',
            'is_admin' => '0',
            'password' => bcrypt('123456'),
        ]);
        DB::table('users')->insert([
            'first_name' => 'User6',
            'last_name' => 'User6',
            'email' => 'user6@email.com',
            'dob' => '1997-02-01',
            'gender' => 'Male',
            'annual_income' => '200000-300000',
            'occupation' => 'Private job',
            'family_type' => 'Nuclear family',
            'manglik' => 'No',
            'is_admin' => '0',
            'password' => bcrypt('123456'),
        ]);
        DB::table('users')->insert([
            'first_name' => 'User7',
            'last_name' => 'User7',
            'email' => 'user7@email.com',
            'dob' => '1996-02-01',
            'gender' => 'Female',
            'annual_income' => '100000-200000',
            'occupation' => 'Private job',
            'family_type' => 'Nuclear family',
            'manglik' => 'Yes',
            'is_admin' => '0',
            'password' => bcrypt('123456'),
        ]);
        DB::table('users')->insert([
            'first_name' => 'User8',
            'last_name' => 'User8',
            'email' => 'user8@email.com',
            'dob' => '1997-02-01',
            'gender' => 'Female',
            'annual_income' => '200000-3000000',
            'occupation' => 'Private job',
            'family_type' => 'Nuclear family',
            'manglik' => 'Yes',
            'is_admin' => '0',
            'password' => bcrypt('123456'),
        ]);
        DB::table('users')->insert([
            'first_name' => 'User9',
            'last_name' => 'User9',
            'email' => 'user9@email.com',
            'dob' => '1998-05-02',
            'gender' => 'Male',
            'annual_income' => '200000-400000',
            'occupation' => 'Private job,Government Job',
            'family_type' => 'Nuclear family,Joint family',
            'manglik' => 'Yes',
            'is_admin' => '0',
            'password' => bcrypt('123456'),
        ]);
        DB::table('users')->insert([
            'first_name' => 'User10',
            'last_name' => 'User10',
            'email' => 'user10@email.com',
            'dob' => '1994-10-05',
            'gender' => 'Female',
            'annual_income' => '200000-600000',
            'occupation' => 'Private job,Government Job',
            'family_type' => 'Nuclear family,Joint family',
            'manglik' => 'No',
            'is_admin' => '0',
            'password' => bcrypt('123456'),
        ]);

       
    }
}
