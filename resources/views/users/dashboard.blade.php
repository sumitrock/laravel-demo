@extends('layout')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-11">
            <div class="card">
                <div class="card-header">Welcome {{ucfirst($user_details->first_name)}} {{ucfirst($user_details->last_name)}}</div>
  
                <div class="card-body">
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
  
                   
                </div>
                @if($user_details)
                <table border = "0" width = "80%" >
                     <tr>
                        <td>
                           <table border = "1" width = "80%" style="text-align: center;margin-left: 20%;">
                              <tr>
                                <th>Name</th>
                                <th>Email</th>
                                 <th>Dob</th>
                                 <th>Manglik</th>
                                 <th>Occupaiton</th>
                                 <th>FamilyType</th>
                                 <th>Gender</th>
                              </tr>
                              <tr>
                                <td>{{$user_details->first_name}} {{$user_details->last_name}}</td>
                                <td>{{$user_details->email}}</td>
                                <td>{{$user_details->dob}}</td>
                                <td>{{$user_details->manglik}}</td>
                                <td>{{$user_details->occupation}}</td>
                                <td>{{$user_details->family_type}}</td>
                                <td>{{$user_details->gender}}</td>
                              </tr>
                             
                           </table>
                        </td>
                     </tr>
                  </table>
            
                 @endif
               
                <table id="table_user" class="display" style="width:100%">
                    <thead>  
                        <tr>
                            <th>S. No.</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            <th>Date Of Birth</th>
                            <th>Gender</th>
                            <th>Annual Income(₹)</th>
                            <th>Occupation</th>
                            <th>Family Type</th>
                            <th>Manglik</th>
                            <th>Match Percentage(%)</th>
                        </tr>
                    </thead>
                    <tbody>
                     @if($user_list_array)
                         @foreach($user_list_array as $key => $value)     
                             <tr>
                              <td>{{$key+1}}</td>
                              <td>{{$value['first_name']}}</td>
                              <td>{{$value['last_name']}}</td>
                              <td>{{$value['email']}}</td>
                              <td>{{$value['dob']}}</td>
                              <td>{{$value['gender']}}</td>
                              <td>{{$value['annual_income']}}</td>
                              <td>{{$value['occupation']}}</td>
                              <td>{{$value['family_type']}}</td>
                              <td>{{$value['manglik']}}</td>
                              <td>{{$value['match_percentage']}}%</td>
                            </tr>
                         @endforeach   
                      @endif   

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    DataTable = $('#table_user').DataTable({


    });

</script>
@endsection